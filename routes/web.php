<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', function () {
    return redirect('users/create');
})->middleware('auth');

Auth::routes();
Route::resource('users', 'UserController')->middleware('auth');
Route::resource('comments', 'CommentController')->middleware('auth');
Route::resource('announcements', 'AnnouncementController')->middleware('auth');
Route::post('users-update/{id}', 'UserController@update')->middleware('auth');
Route::post('/users/ratingUpdate/{id}', 'UserController@ratingUpdate')->middleware('auth');

