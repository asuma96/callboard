<?php

namespace App\Http\Controllers;

use App\Announcement;
use App\Http\Requests\CreateAnnouncementRequest;
use App\Image;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Intervention\Image\Facades\Image as ImageInt;
use Illuminate\Support\Facades\Auth;

class AnnouncementController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $announcements = Announcement::with('images')->paginate(20);
        return view('welcome', compact('announcements'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('pages/announcemenets_create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateAnnouncementRequest $request)
    {
        $announcements=Announcement::where('id', Auth::user()->id)->create(['user_id' => Auth::user()->id, 'name' => $request->name, 'description' => $request->description]);
        $path = public_path() . '/upload\\';
        $file = $request->file('file');
        $filename = Str::random(20) . '.' . $file->getClientOriginalExtension() ?: 'png';

        $img = ImageInt::make($file);
        $img->resize(200, 200)->save($path . $filename);
        Image::create(['title' => $request->name, 'img' => $filename, 'user_id' => $announcements->user_id,'announcement_id' => $announcements->id]);
        $file->move(storage_path('app/public/images'), $filename);
        return redirect('/announcements');
    }

    /**
     * Display the specified resource.
     *
     * @param \App\Announcement $announcement
     * @return \Illuminate\Http\Response
     */
    public function show(Announcement $announcement)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param \App\Announcement $announcement
     * @return \Illuminate\Http\Response
     */
    public function edit(Announcement $announcement)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Announcement $announcement
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Announcement $announcement)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Announcement $announcement
     * @return \Illuminate\Http\Response
     */
    public function destroy(Announcement $announcement)
    {
        //
    }
}
