<?php

namespace App\Http\Controllers;

use App\Announcement;
use App\Comment;
use App\Http\Requests\CreateUserRequest;
use App\Image;
use App\User;
use Illuminate\Http\Request;
use Intervention\Image\Facades\Image as ImageInt;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (User::query()->where('id', Auth::user()->id)->first()['surname'] != null) {
            $comments = Comment::with('users')->paginate(10);
            $user = User::query()->where('id', Auth::user()->id)->first();
            $images = Image::query()->where('user_id', $user->id)->get();
            $announcements = Announcement::with('images')->where('user_id', $user->id)->paginate(20);
            return view('/pages/my_account', compact('user', 'images', 'comments', 'announcements'));
        }
        return redirect('users/create');

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $user = User::select()->where('id', Auth::user()->id)->first();
        return view('pages/personal', compact('user'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(CreateUserRequest $request, $id)
    {
        User::where('id', $id)->update($request->except(['_token', 'file']));

        if ($request->file('file')) {
            $path = public_path() . '/upload\\';
            $file = $request->file('file');
            $filename = Str::random(20) . '.' . $file->getClientOriginalExtension() ?: 'png';

            $img = ImageInt::make($file);
            $img->resize(200, 200)->save($path . $filename);
            if (Image::where('user_id', $id)->first()) {
                Image::where('user_id', $id)->update(['img' => $filename]);
            } else {
                Image::create(['title' => $request->name, 'img' => $filename, 'user_id' => $id]);
            }
            $file->move(storage_path('app/public/images'), $filename);
            return redirect('/users');
        }
        return redirect('/users');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    /**
     * Update rating for this user.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function ratingUpdate(Request $request, $id)
    {
        User::where('id', $id)->update($request->except(['_token']));
        return redirect('users');
    }
}
