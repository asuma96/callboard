<?php

namespace App;

use App\User;
use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    protected $table = 'comments';
    protected $fillable = [
        'text', 'date', 'user_id'
    ];
    public $timestamps = false;

    public function users()
    {
        return $this->belongsTo(User::class);
    }
}
