<?php

namespace App;

use App\User;
use App\Announcement;
use Illuminate\Database\Eloquent\Model;

class Image extends Model
{
    protected $table = 'images';
    protected $fillable = [
        'title', 'img', 'user_id', 'announcement_id'
    ];

    public function users()
    {
        return $this->belongsTo(User::class);
    }

    public function announcements()
    {
        return $this->belongsTo(Announcement::class);
    }
}
