<?php

namespace App;

use App\User;
use App\Image;
use Illuminate\Database\Eloquent\Model;

class Announcement extends Model
{
    protected $table = 'announcements';
    protected $fillable = [
        'name', 'description', 'user_id', 'announcement_id'
    ];

    public function users()
    {
        return $this->belongsTo(User::class);
    }

    public function images()
    {
        return $this->hasOne(Image::class);
    }
}
