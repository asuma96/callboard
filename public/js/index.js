$('document').ready(function () {
    $('.users-form').validate({
        rules: {
            name: 'required',
            surname: 'required',
            img: {
                required: true,
                extension: "png|jpeg|jpg",
            },
            phone: {
                required: true,
                minlength: 16
            }
        },
        messages: {
            name: 'Введите название',
            surname: 'Введите фамилию',
            img: 'добавьте картинку',
            phone: {
                required: 'Введите номер телефона',
                minlength: 'Номер должен состоять не менее, чем из 10 цифр'
            }
        },

        errorElement: 'div',
        errorLabelContainer: "#messageBox",
    });
    if ($('[name="phone"]').length) {
        $('[name="phone"]').mask('+7 (000) 000-00-00');
    }
});
