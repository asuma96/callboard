$('document').ready(function () {
    $('.announcements-form').validate({
        rules: {
            description: {
                required: true,
                maxlength: 175
            },
            name: 'required',
        },
        messages: {
            name: 'Введите название',
            text: {
                required: 'Введите описание товара',
                maxlength: 'Описание может содержать максимум 175 символов'
            }
        },
        errorElement: 'div',
        errorLabelContainer: "#messageBox",
    });
});





