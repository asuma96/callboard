$('document').ready(function () {
    $('.comments-form').validate({
        rules: {
            text: {
                required: true,
                maxlength: 190
            }
        },
        messages: {
            text: {
                required: 'Введите комментарий',
                maxlength: 'Комментарий может содержать максимум 190 символов'
            }
        },
        errorElement: 'div',
        errorLabelContainer: "#messageBox",
    });
});

