@extends('layouts.app')
@section('content')
    <div class="container">
        <div class="row">
            <div class="user-detail__top col-md-6">
                <div class="user-detail__top-subheading">{{ $user->name}} {{ $user->surname }}</div>
            </div>
            <div class="row">
                <div class="col-md-6 ">
                    <div class="user-detail__description-img ">
                        <img src="{{asset('storage/images/'.$images[0]->img)}}" alt="" class="my_account-avatar">
                    </div>
                    <form action="/users/ratingUpdate/{{$user->id}}" class="add-form users-form" method="POST"
                          enctype="multipart/form-data">
                        @csrf
                        <div id="reviewStars-input">
                            <input id="star-4" type="radio" name="rating" value="5"/>
                            <label title="gorgeous" for="star-4"></label>

                            <input id="star-3" type="radio" name="rating" value="4"/>
                            <label title="good" for="star-3"></label>

                            <input id="star-2" type="radio" name="rating" value="3"/>
                            <label title="regular" for="star-2"></label>

                            <input id="star-1" type="radio" name="rating" value="2"/>
                            <label title="poor" for="star-1"></label>

                            <input id="star-0" type="radio" name="rating" value="1"/>
                            <label title="bad" for="star-0"></label>
                        </div>
                        <input type="submit" class="btn btn-primary full-width allign-submit" value="Оценить">
                    </form>
                </div>
                <div class="col-md-6">
                    <div>
                        <h2>Номер телефона</h2>
                        <h1>{{$user->phone}}</h1>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="comments">
                <h2>Комментарии:</h2>
                @foreach($comments as $comment)
                    <div>
                        <p>
                            {{$comment['text']}}
                            <br>
                            {{$comment['date']}}
                        </p>
                    </div>
                    <div class="news__links">
                    </div>
                @endforeach
                {{ $comments->links('vendor.pagination.trb-default') }}

                <form action="/comments" class="comments-form add-form users-form" method="POST"
                      enctype="multipart/form-data">
                    @csrf
                    <div class="col-md-6 comment_pading">
                            <textarea id="text" class="comment_area comments"
                                      type="text" name="text" placeholder="Комментарий"></textarea>
                    </div>
                    <div id="messageBox">
                    </div>
                    <input type="submit" class="btn btn-primary full-width comments" value="Отправить">
                </form>
            </div>
        </div>
        <div class="container">
            <div class="announcements">
                <h1 class="comments d-flex justify-content-center">Объявления</h1>
                <div class="announcement">
                    <div class="row">
                        @foreach($announcements as $announcement)
                            <div class="col-lg-4 col-md-6">
                                <div class="announcement__item-wrapper">
                                    <div class="announcement__item">
                                        <img src="{{ asset('storage/images/'.$announcement->images['img']) }}" alt=""
                                             class="announcement__img">
                                        <div class="announcement__bottom-info">
                                            <div class="announcement__name">{{ $announcement->name }}</div>
                                            <div id="data"
                                                 class="announcement__description">{{ $announcement->description }}</div>
                                            <div class="announcement__price-from">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endforeach

                    </div>
                    <div class="news__links">
                        {{ $announcements->links('vendor.pagination.trb-default') }}
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
@endsection
