@extends('layouts.app')
@section('content')
    <form action="/announcements" class="add-form announcements-form" method="POST"
          enctype="multipart/form-data">
        @csrf
        <div class=" container">
            <div class="col-md-6 center_image">
                <div class="form-group">
                    <label class="label add-file add-file__big">
                        <div class="add-file__img"></div>
                        <span class="add-file__title">Фото объявления</span>
                        <input id="img" type="file" name="file">
                    </label>
                </div>
            </div>

            <div class="col-md-6">
                <div class="form-group with-label">
                    <label for="name">Название товара</label>
                    <input id="name" class="modalWin__feeadback-input" name="name" type="text"
                           placeholder="Название" value="">
                </div>
                <div class="col-md-6 comment_pading">
                            <textarea id="description" class="comment_area comments"
                                      type="text" name="description" placeholder="Описание товара"></textarea>
                </div>
                <div id="messageBox">

                </div>
                @if (count($errors) > 0)
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                <input type="submit" class="btn btn-primary full-width" value="Отправить">
            </div>
        </div>
    </form>
    @endsection
