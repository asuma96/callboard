@extends('layouts.app')
@section('content')
    <form action="/users-update/{{$user->id}}" class="add-form users-form" method="POST"
          enctype="multipart/form-data">
        @csrf
        <div class=" container">
            <div class="col-md-6 center_image">
                <div class="form-group">
                    <label class="label add-file add-file__big">
                        <div class="add-file__img"></div>
                        <span class="add-file__title">Загрузить аватар</span>
                        <input id="img" type="file" name="file" onload="alert('Файл существует!');"
                               onerror="alert('Файл не найден')">

                    </label>

                </div>
            </div>

            <div class="col-md-6">
                <div class="form-group with-label">
                    <label for="name">Имя</label>
                    <input id="name" class="modalWin__feeadback-input" name="name" type="text"
                           placeholder="Имя" value="{{$user->name}}">
                </div>
                <div class="form-group with-label">
                    <label for="surname">Фамилия</label>
                    <input id="surname" class="modalWin__feeadback-input" name="surname"
                           type="text" placeholder="Фамилия" value="{{$user->surname ?? ""}}">
                </div>
                <div class="form-group with-label">
                    <label for="phone">Телефон</label>
                    <input id="phone" class="modalWin__feeadback-input" name="phone"
                           type="text" placeholder="" value="{{$user->phone ?? ""}}">
                </div>
                <div id="messageBox">

                </div>
                @if (count($errors) > 0)
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif

                <input type="submit" class="btn btn-primary full-width" value="Отправить">
            </div>
        </div>
    </form>

@endsection
