@extends('layouts.app')

@section('content')
    <div class="announcement">
        <div class="container">
            <h2 class="announcement__found">На текущей странице представлено предложений : <span
                    class="announcement__found-count">{{count($announcements)}}</span></h2>
            <div class="row">
                @foreach($announcements as $announcement)
                    <div class="col-lg-4 col-md-6">
                        <div class="announcement__item-wrapper">
                            <div class="announcement__item">
                                <img src="{{ asset('storage/images/'.$announcement->images->img) }}" alt=""
                                     class="announcement__img">
                                <div class="announcement__bottom-info">
                                    <div class="announcement__name">{{ $announcement->name }}</div>
                                    <div id="data"
                                         class="announcement__description">{{ $announcement->description }}</div>
                                    <div class="announcement__price-from">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
            <div class="news__links">
                {{ $announcements->links('vendor.pagination.trb-default') }}
            </div>
        </div>
    </div>
@endsection
