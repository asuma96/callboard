{{--<div class="pagination">
    <a href="#" class="pagination__prev"></a>

    <a href="#" class="pagination__page-num">2</a>
    <a href="#" class="pagination__page-num active">3</a>
    <a href="#" class="pagination__page-num">4</a>
    <a href="#" class="pagination__page-num pagination__page-etc">...</a>
    <a href="#" class="pagination__page-num">105</a>

    <a href="#" class="pagination__next"></a>
</div>--}}



@if ($paginator->hasPages())
    {{--<ul class="pagination" role="navigation">--}}
    <div class="pagination" role="navigation">
        {{-- Previous Page Link --}}
        @if ($paginator->onFirstPage())
            {{--<li class="page-item disabled" aria-disabled="true" aria-label="@lang('pagination.previous')">
                <span class="page-link" aria-hidden="true">&lsaquo;</span>
            </li>--}}
            <div class="pagination__prev disabled"></div>
        @else
            {{--<li class="page-item">
                <a class="page-link" href="{{ $paginator->previousPageUrl() }}" rel="prev" aria-label="@lang('pagination.previous')">&lsaquo;</a>
            </li>--}}
            <a href="{{ $paginator->previousPageUrl() }}" rel="prev" class="pagination__prev"></a>
        @endif

        {{-- Pagination Elements --}}
        @foreach ($elements as $element)
            {{-- "Three Dots" Separator --}}
            @if (is_string($element))
                {{--<li class="page-item disabled" aria-disabled="true"><span class="page-link">{{ $element }}</span></li>--}}
                <div class="pagination__page-num">{{ $element }}</div>
            @endif

            {{-- Array Of Links --}}
            @if (is_array($element))
                @foreach ($element as $page => $url)
                    @if ($page == $paginator->currentPage())
                        {{--<li class="page-item active" aria-current="page"><span class="page-link">{{ $page }}</span></li>--}}
                        <div class="pagination__page-num active" aria-current="page">{{ $page }}</div>
                    @else
                        {{--<li class="page-item"><a class="page-link" href="{{ $url }}">{{ $page }}</a></li>--}}
                        <a href="{{ $url }}" class="pagination__page-num">{{ $page }}</a>
                    @endif
                @endforeach
            @endif
        @endforeach

        {{-- Next Page Link --}}
        @if ($paginator->hasMorePages())
            {{--<li class="page-item">
                <a class="page-link" href="{{ $paginator->nextPageUrl() }}" rel="next" aria-label="@lang('pagination.next')">&rsaquo;</a>
            </li>--}}
            <a href="{{ $paginator->nextPageUrl() }}" rel="next" class="pagination__next"></a>
        @else
            {{--<li class="page-item disabled" aria-disabled="true" aria-label="@lang('pagination.next')">
                <span class="page-link" aria-hidden="true">&rsaquo;</span>
            </li>--}}
            <div class="pagination__next disabled"></div>
        @endif
    </div>
    {{--</ul>--}}
@endif
